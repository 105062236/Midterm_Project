# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Forum
* Key functions (add/delete)
    1. user page: profile.html 以及 profilechg.html
    2. post page: release.html
    3. post page list: mainpage.html
    4. leave comment page: comment.html
* Other functions (add/delete)
    1. 電子信箱認證功能: 在更改密碼時，須至信箱點選郵件，輸入新的密碼，才可以更改，確保填寫信箱便非虛假，以及增添安全性。
    2. [xxx]
    3. [xxx]
    4. [xxx]

## Basic Components
|Component|Score|Y|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
    開頭的地方有個會動的笑臉(css animation)
    可用fb及google登入
    當有人發送文章時，會推播，讓其他人知道

    點選上方的nav bar中的new post便可撰寫要發布的內容及主題，內容在顯示時會自動換行而不超出畫面，另外在顯示時會列出當時發文的時間。
    
    profile的部分，可以填入姓名，性別，常用信箱，以及頭像的url，填寫url後，留言以及發布文章便會出現頭像。
    
    進入mainpage之後，點選每則主題的topic，便可開始留言，按下submit後，留言就會出現在文章下方，同樣會有留言的時間，另外mainpage只會顯示主題即發文者，不會顯示內容。

    
## Security Report (Optional)
    1.  在留言時，為避免被惡意植入連結，像是音效檔，惡意超連結，因此將存放留言的方式，從innerHTML改成innerText。
    2.  留言及觀看文章必須是登入中的用戶。