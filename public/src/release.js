function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {

        var account = document.getElementById('Account');
        // Check user login
        if (user) {
            user_email = user.email;
            account.innerHTML = "Hello " + user.email + ' !';
            var btnLogout = document.getElementById("logout-btn");
            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut().then(function(){
                    alert("success!");
                })
                .catch(function(err){
                    alert("error");
                });
            });
            
        } else {
        }
    });

    post_btn = document.getElementById('post_btn');
    post_title = document.getElementById('title');
    post_txt = document.getElementById('content');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "" && post_title.value != "") {
            var today=new Date();
　          var time = today.getFullYear()+'年'+

            (today.getMonth()+1)+'月'+

            today.getDate()+'日('+

            today.getHours()+':'+today.getMinutes()+

            ')';
            console.log(time);
            var loginUser = firebase.auth().currentUser;
            var userRef = firebase.database().ref('Users/' + loginUser.uid);
            var user_icon = 'https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg';
            userRef.once('value').then(function(snapshot){
                var data = snapshot.val();

                var exi = snapshot.child("icon").exists();
                console.log(exi);

                if(exi) user_icon = data.icon;
            })
            
            .then(function(){
                var postRef = firebase.database().ref('com_list');
            
                postRef.once("value")
                    .then(function(snapshot){
                        var i = snapshot.numChildren();
                        console.log(user_icon);

                        
                            
                        var postsRef = firebase.database().ref('com_list/' + i).set({
                            title  : post_title.value,
                            content: post_txt.value,
                            user   : user_email,
                            icon   : user_icon,
                            time   : time
                            
                            
                        })
                        if ("Notification" in window){
                            let ask = Notification.requestPermission();
                            ask.then(permission=> {
                                if(permission == "granted")
                                {

                                    console.log(post_title.value);
                                    let msg = new Notification("!!!", {
                                        body: user_email + '  post  ' + post_title.value,
                                        icon: user_icon
                                    });
                                    msg.addEventListener("click", event=> {
                                        window.location.href = './comment.html?'+ i +'';
                                    });
                                    post_txt.value = '';
                                    post_title.value = '';
                                }
                            });
                        }   
                    }) 
                window.location = "../dist/mainpage.html";   
                alert("Post successfully!");  
            })

             
        }
        else{
            alert("Please input title and content");
        }
    });

    user_email = '';

    

}

window.onload = function () {
    init();
    
};