function init() {
    var user_email = '';

    var change = document.getElementById('change');
    var name = document.getElementById('name');
    var sex = document.getElementById('sex');
    var email = document.getElementById('email');
    var icon = document.getElementById('icon');

    firebase.auth().onAuthStateChanged(function (user) {
        var account = document.getElementById('Account');
        // Check user login
        if (user) {
            account_email = user.email;
            account.innerHTML = "Hello " + user.email + ' !';
            
            var btnLogout = document.getElementById("logout-btn");
            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut().then(function(){
                    alert("success!");
                })
                .catch(function(err){
                    alert("error");
                });
            });

            var loginUser = firebase.auth().currentUser;
            console.log(firebase.auth().currentUser);
            var userRef = firebase.database().ref('Users/' + loginUser.uid);

            userRef.once('value').then(function(snapshot){
                var data = snapshot.val();
                if(data.email !== '') email.innerText = '常用信箱:  ' + data.email;
                if(data.name !== '')  name.innerText  = '姓名:  ' + data.name;
                if(data.sex !== '')   sex.innerText   = '性別:  ' + data.sex;
                if(data.icon !== '')  icon.innerHTML  = '<img src='+data.icon+'>';
            })
            
        } else {
        }
        
    });

    
    change.addEventListener('click', function(user) {
        console.log(firebase.auth().currentUser);
        window.location.href = '../dist/profilechg.html';        
    });

    


}

window.onload = function () {
    init();
};