function init() {
    var user_email = '';
    var comment_count = 0;
    firebase.auth().onAuthStateChanged(function (user) {

        var account = document.getElementById('Account');
        // Check user login
        if (user) {
            user_email = user.email;
            account.innerHTML = "Hello " + user.email + ' !';
            var btnLogout = document.getElementById("logout-btn");
            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut().then(function(){
                    alert("success!");
                })
                .catch(function(err){
                    alert("error");
                });
            });
            
        } else {
        }
        
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('content');
    var user_icon = '';
    var today;
    var time;
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {

            today=new Date();
　          time = today.getFullYear()+'年'+

            (today.getMonth()+1)+'月'+

            today.getDate()+'日('+

            today.getHours()+':'+today.getMinutes()+

            ')';

            var loginUser = firebase.auth().currentUser;
            var userRef = firebase.database().ref('Users/' + loginUser.uid);
            var user_icon = 'https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg';
            userRef.once('value').then(function(snapshot){
                var data = snapshot.val();

                var exi = snapshot.child("icon").exists();
                console.log(exi);
                if(exi) user_icon = data.icon;
            })
            .then(function(){var href = location.href;
                var i = href.substring(href.indexOf("?")+1);
                var postRef = firebase.database().ref('com_list/' + i + '/comment');
                var set = {
                    txt : post_txt.value,
                    user: user_email,
                    time: time,
                    icon: user_icon
                };
                var newpost =postRef.push(set);
                alert("Post successfully!");
                post_txt.value = '';   
    
                var loginUser = firebase.auth().currentUser;
                var userRef = firebase.database().ref('Users/' + loginUser.uid);
                
                userRef.once('value').then(function(snapshot){
                    var data = snapshot.val();
                    user_icon = data.icon;
                })
            })
            
        }
        else{
            alert("Please input title and content");
        }
    });
    var href = location.href;
    var i = href.substring(href.indexOf("?")+1);
    var postRef = firebase.database().ref('com_list/' + i);
    var post = '';
    

    postRef.once('value').then(function (snapshot) {
        
        var childData = snapshot.val();
        var str_before_username = "<div id='author' class=' str_bef rounded box-shadow'><div class='media text-danger pt-3'><img src="+childData.icon+" alt='' class='mr-2 rounded' style='height:70px; width:70px;><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block'>";
        var str_after_content = "</strong></div></div>";
        post = str_before_username  
               + 'author:  ' + childData.user  +  '<br>' 
               + 'Post time: ' + childData.time + '<br>'
               + 'Topic:  '  +  childData.title  
               +  str_after_content;

        str_before_username = "<div id='content' class='text-danger' >";
        post = post + str_before_username + 'Content:' + '<br>' + childData.content + str_after_content;
    })
    .then(function(){
        var postsRef = firebase.database().ref('com_list/' + i + '/comment');
        var amp = [];
        var total_post = [];
        var temp = 0;
        var first_count = 0;
        var second_count = 0;
        //comment

        postsRef.once('value')
            .then(function (snapshot) {
                
                snapshot.forEach(function(childSnapshot){
                    var childData = childSnapshot.val();
                    
                    var str_before_username = "<div id='author' class=' str_bef rounded box-shadow'><div class='media text-danger pt-3'><img src="+childData.icon+" alt='' class='mr-2 rounded' style='height:70px; width:70px;><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block'>";
                    var str_after_content = "</strong></div></div>";

                    console.log(childData.icon);
                    var temp2=temp.toString();

                    total_post[total_post.length] = str_before_username  
                                                    + 'author:  ' + childData.user
                                                    + '  Post time: ' + childData.time + '<br>'
                                                    + 'comment:  '  +  "<span id="+temp2+"></span>"  
                                                    +  str_after_content;

                    
                    
                    first_count += 1;

                    amp[amp.length]=childData.txt;
                    temp+=1;
                });
                document.getElementById('post_list').innerHTML = post + total_post.join('');
                for(a=0;a<temp;a++){
                    var temp2=a.toString();
                    document.getElementById(temp2).innerText=amp[a];

                }

                postsRef.on('child_added',function(data){
                    second_count += 1;
                    if(second_count > first_count){
                        var temp2=temp.toString();
                        var childData = data.val();
                        

                        var str_before_username = "<div id='author' class=' str_bef rounded box-shadow'><div class='media text-danger pt-3'><img src="+childData.icon+" alt='' class='mr-2 rounded' style='height:70px; width:70px;><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block'>";
                        var str_after_content = "</strong></div></div>";

                        total_post[total_post.length] = str_before_username  
                                                        + 'author:  ' + childData.user  
                                                        + 'Post time: ' + today + '<br>'
                                                        + 'comment:  '  +  "<span id="+temp2+"></span>"  
                                                        +  str_after_content;
                        
                        amp[amp.length]=childData.txt;
                        temp+=1;
                        document.getElementById('post_list').innerHTML = post + total_post.join('');

                        for(a=0;a<temp;a++){
                            var temp2=a.toString();
                            document.getElementById(temp2).innerText=amp[a];

                        }

                    }
                });
            })
        })
        user_email = '';

        
}

window.onload = function () {
    init();
};