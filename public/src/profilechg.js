function init() {
    var account_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var account = document.getElementById('Account');
        // Check user login
        if (user) {
            account_email = user.email;
            account.innerHTML = "Hello " + user.email + ' !';
            
            var btnLogout = document.getElementById("logout-btn");
            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut().then(function(){
                    alert("success!");
                })
                .catch(function(err){
                    alert("error");
                });
            });
            
        } else {
        }
        
    });

    save = document.getElementById("Save");
    chgPW = document.getElementById("chgPassWord");

    inpName = document.getElementById("InputName");
    inpSex = document.getElementById("InputSex");
    inpEmail = document.getElementById("InputEmail");

    inpIcon = document.getElementById("InputIcon");

    
    
    save.addEventListener('click', function(user) {
        loginUser = firebase.auth().currentUser;
        console.log(loginUser.uid);
        if(inpEmail.value != '' && inpSex.value != '' && inpSex.value != ''){
            var userRef = firebase.database().ref('Users/' + loginUser.uid).set({
                name: inpName.value,
                email: inpEmail.value,
                sex: inpSex.value,
                icon: inpIcon.value
            })
            alert("Update Success");
            window.location.href = '../dist/profile.html';
        }
        else{
            alert("Please fill your information completely!");
        }
        
    });


    chgPW.addEventListener('click', function() {
        var auth = firebase.auth();
        var emailAddress = account_email;
        console.log(emailAddress);
        auth.sendPasswordResetEmail(emailAddress).then(function() {
        // Email sent.
            alert("Please cheakout your email!");
        }).catch(function(error) {
        // An error happened.
        });

    });


}
window.onload = function () {
    init();
};